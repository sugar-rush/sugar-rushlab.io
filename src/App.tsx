import "./App.css";
import Footer from "./components/footer/Footer";
import Gallery from "./components/gallery/Gallery";
import Hero from "./components/hero/Hero";
import Intro from "./components/intro/Intro";
import Menu from "./components/menu/Menu";
import Navbar from "./components/navbar/Navbar";

function App() {
  return (
    <div className="">
      <Navbar></Navbar>
      <Hero></Hero>
      <Intro></Intro>
      <Gallery />
      <Menu></Menu>
      <Footer></Footer>
    </div>
  );
}

export default App;
