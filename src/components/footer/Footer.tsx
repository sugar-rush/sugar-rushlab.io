import React from "react";
import { tw } from "twind";
import FooterElements from "./FooterElements";

function Footer() {
  return (
    <>
      <div className={tw`w-screen min-h-[70vh] bg-black text-white bottom-0 `}>
        <section
          className={tw`p-[8%] text-white flex flex-col items-center md:flex-row md:justify-evenly`}
        >
          <FooterElements></FooterElements>
        </section>
        <section
          className={tw`text-center text-white italic bottom-[10px] flex justify-center`}
        >
          © Reserved - 2022{" "}
        </section>
      </div>
    </>
  );
}

export default Footer;
