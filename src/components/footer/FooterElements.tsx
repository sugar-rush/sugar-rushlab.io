import React from "react";
import { tw } from "twind";

function FooterElements() {
  return (
    <div className={tw`mx-3 w-[50%] md:w-[25%] my-2 text-center`}>
      <div className={tw` w-full `}>
        <div className={tw`h-6 text-[20px] text-[#c19f5f]`}>Contact</div>
        <br />
        <div className={tw`font-light text-[gray] flex justify-between p-5`}>
          <div>Instagram</div>
          <div className={tw`flex justify-center m-[5px]`}>
            <a href="https://www.instagram.com/sugar_rush42/" target="_blank">
              <img
                src="/images/instagram.png"
                className={tw`w-[20px]`}
                alt=""
              />
            </a>
          </div>
        </div>
        <div className={tw`font-light text-[gray] flex justify-between p-5`}>
          <div>Whatsapp</div>
          <div className={tw`flex justify-center m-[5px]`}>
            <a href="https://wa.me/c/919511819131" target="_blank">
              <img src="/images/whatsapp.png" className={tw`w-[20px]`} alt="" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FooterElements;
