import React from "react";
import { tw } from "twind";
function Intro() {
  return (
    <div className={tw`relative w-full h-full`}>
      <div className={tw` absolute top-0 left-0 right-0 bottom-0 -z-[1] `}>
        <img
          className={tw`w-full h-full object-cover`}
          src="images/image2.jpg"
          alt=""
        />
      </div>

      <div
        className={tw`flex flex-col items-center md:flex-row md:justify-evenly md:items-start w-full h-full z-50`}
      >
        <div className={tw`max-w-screen-sm my-10`}>
          <img src="/images/basket.png" className={tw`w-full h-full`} alt="" />
        </div>
        <div className={tw`text-center my-10 p-3`}>
          <div
            className={tw`text-sm font-light text-gray-700 text-[#ce873a] font-black`}
            style={{
              fontFamily: '"Cormorant Garamond", serif',
            }}
          >
            WELCOME TO
            <div className={tw`text-xl text-[#9A7E49] text-[40px] mt-1`}>
              Sugar Rush
            </div>
          </div>
          <div
            className={tw`max-w-screen-sm italic text-[#cca881] leading-6 font-light text-lg my-10`}
          >
            {/* " A sugar rush is an experience of high energy after eating or
            drinking a considerable amount of sugar in a short period of time,
            often associated with hyperactive children " */}

            <img
              src="/images/AboutsugarRush.jpeg"
              className={tw`w-full h-full shadow-lg shadow-indigo-500/40`}
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Intro;
