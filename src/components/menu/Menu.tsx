import React from "react";
import { tw } from "twind";
function Intro() {
  return (
    <div className={tw`relative w-full h-full`}>
      <div className={tw` absolute top-0 left-0 right-0 bottom-0 -z-[1] `}>
        <img
          className={tw`w-full h-full object-cover`}
          src="images/image2.jpg"
          alt=""
        />
      </div>
      <div className={tw`text-center text-white`}>
        <div
          style={{
            fontFamily: '"Cormorant", serif',
          }}
          className={tw`text-[35px] text-bold md:text-[48px] text-black`}
        >
          Menu Card
        </div>
      </div>
      <div
        className={tw`flex flex-col items-center md:flex-row md:justify-evenly  w-full h-full z-50 p-8 content-centers`}
      >
        <div
          className={tw`m-2  shadow-lg shadow-indigo-500/40 cursor-pointer `}
        >
          <img src="/images/menu1.jpeg" alt="" />
        </div>
        <div className={tw`m-2  shadow-lg shadow-indigo-500/40 cursor-pointer`}>
          <img src="/images/menu2.jpeg" alt="" />
        </div>
        <div className={tw`m-2  shadow-lg shadow-indigo-500/40 cursor-pointer`}>
          <img src="/images/menu3.jpeg" alt="" />
        </div>
      </div>
    </div>
  );
}

export default Intro;
