import React from "react";
import { tw } from "twind";

function GalleryElement({ src }: any) {
  return (
    <div
      className={tw`max-w-[190px] md:max-w-[280px] m-2 overflow-hidden cursor-pointer`}
    >
      <img
        src={src}
        className={tw`w-full h-full object-cover hover:scale-[1.2] ease-out duration-500`}
        alt=""
      />
      {/* <div
        className={tw`border-1 border-[#b79659] absolute top-0 right-0 bottom-0 left-0 bg-[red]`}
      ></div> */}
    </div>
  );
}

export default GalleryElement;
