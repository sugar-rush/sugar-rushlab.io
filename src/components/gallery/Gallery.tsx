import React, { useLayoutEffect, useState } from "react";
import { tw } from "twind";
import GalleryElement from "./GalleryElement";

const galleryImgs = [
  "/images/gallery-1.jpg",
  "/images/gallery-2.jpg",
  "/images/gallery-3.jpg",
  "/images/gallery-4.jpg",
];

function Gallery() {
  const scrollRef = React.useRef<any>(null);

  const [bgTop, setBgTop] = useState(0);
  const onScroll = () => {
    setBgTop(scrollRef.current?.getBoundingClientRect().top * 0.2);
  };

  useLayoutEffect(() => {
    window.addEventListener("scroll", onScroll);

    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  return (
    <div className={tw`relative w-screen min-h-screen`} ref={scrollRef}>
      <div
        className={tw`absolute bg-fixed -z-[1] duration-100 w-screen  h-full`}
        style={{
          backgroundPosition: `center ${bgTop}px`,
          backgroundImage: `url("/images/bg-image.jpg")`,
          backgroundRepeat: "repeat",
        }}
      >
        {/* <img src="/images/bg-image.jpg" className={tw``} alt="" /> */}
      </div>

      <div
        className={tw`w-screen h-full min-h-screen flex flex-col justify-center pt-5`}
        style={{ background: "rgba(0,0,0,.85 )" }}
      >
        <div>
          <div className={tw`text-center text-white my-5`}>
            <div
              style={{
                fontFamily: '"Mulish", serif',
              }}
              className={tw`text-[#c19f5f] tracking-[4px]`}
            >
              GALLERY
            </div>
            <div
              style={{
                fontFamily: '"Cormorant", serif',
              }}
              className={tw`text-[35px] md:text-[48px]`}
            >
              With Creativity and Love
            </div>
          </div>

          <div
            className={tw`w-full h-full  flex flex-wrap justify-evenly items-center`}
          >
            {galleryImgs.map((src) => (
              <GalleryElement src={src} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Gallery;
