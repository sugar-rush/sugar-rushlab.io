import React from "react";
import { apply, tw } from "twind";

const heroTitle = apply`text-white text-[50px] md:text-[80px] font-thin leading-10 font-[Cormorant] text-[#c19e5c] `;

function Hero() {
  return (
    <div className={tw`w-screen h-screen flex justify-center items-center`}>
      {/* <div className={tw`h-full w-full bg-black opacity-0 absolute`}></div> */}
      <img
        className={tw`w-full h-full  object-cover`}
        src="/images/fruits.jpg"
        alt=""
      />
      <div className={tw`absolute`}>
        <div className={tw(heroTitle, "relative ")}>
          <div className={tw`leading-10 text-center`}>
            <div className={tw`mb-3`}>Delecious Cakes</div>
            <div className={tw`mt-6`}>and Choclates</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Hero;
