import React from "react";
import { apply, tw } from "twind";
import Pages from "./Pages";

const container = apply`
 z-50 text-white flex w-[80%] mx-auto justify-between
`;

function Navbar() {
  return (
    <div className={tw`absolute w-full my-7 `}>
      <div className={tw(container)}>
        <div
          className={tw`w-[70px] overflow-hidden rounded-full translate-y-[-10px]`}
          // style={{
          //   filter:
          //     "drop-shadow(0 1px 2px rgb(0 0 0 / 0.1)) drop-shadow(0 1px 1px rgb(0 0 0 / 0.06))",
          // }}
        >
          <a href="/">
            <img src="/images/logo.png" alt="" />
          </a>
        </div>
        <div className={tw`flex`}>
          <Pages title="About" />
          <Pages title="Products" />
          <Pages title="Blog" />
          <Pages title="Contacts" />
        </div>
      </div>
    </div>
  );
}

export default Navbar;
