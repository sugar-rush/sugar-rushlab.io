import React from "react";
import { tw } from "twind";

function Pages({ title }: any) {
  return (
    <div className={tw`mx-5 hover:text-[#c19e5c]`}>
      <a href={`/${title.toLowerCase()}`}>{title}</a>
    </div>
  );
}

export default Pages;
